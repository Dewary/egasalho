import express, { request } from 'express'
import helmet from "helmet";
import path from 'path';
import routes from "./routes"
 
const app = express();

//Helmet para a proteção dos Headers
app.use(helmet());

app.use(routes);

app.use(express.json());

app.use('/uploads', express.static(path.resolve(__dirname, '..', 'uploads')));

app.listen(8080);
