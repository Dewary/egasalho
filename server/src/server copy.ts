import express, { request } from 'express'

const app = express();

app.use(express.json());

//ROTA: Endereço completo da requisição;
//Recurso: qual entidade do sistema esta sendo acessada

//GET: buscar uma ou mais infos do back-end
//POST: Criar uma nova info no back-end
//PUT: Atualizar uma informação existente no back-end
//DELETE: remover info do back

//POST http://localhost:3333/users = Criar um usuário
//GET http://localhost:3333/users = Listar Usuário
//GET http://localhost:3333/users = Buscar dados do usuário com IDs

// request param: parametros que vem da propria rota que identificam um recurso
// Query Param: parametros que vem na prorpia rota geralmente opcionais oara filtros, paginaçao etc
// Request Body: parametros para criação e atualização de informações

const users = [
    'Diego',
    'Cleiton',
    'Robson',
    'Cristiano',
    'Crusoé'
];

app.get('/users', (request, response) => {
    const search = String(request.query.search);
    
    const filteredUsers = search ? users.filter(user => user.includes(search)) : users;

    response.json(filteredUsers);
});

app.get('/users/:id', (request, response) => {
    const id = Number(request.params.id);

    const user = users[id];

    return response.json(user);
});

app.post("/users", (request, response) => {
    const data = request.body;

    console.log(data);
    
    const user = {
      name: data.name,
      email: data.email,      
    };

    return response.json(user);

});  

app.listen(8080);
