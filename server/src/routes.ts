import express from "express";
import knex from './database/connection';

const routes = express.Router();


routes.get("/", async (request, response) => {
    return console.log(request.body)
});

routes.get("/items", async (request, response) => {
    const items = await knex('items').select('*');
    
    const serializedItems = items.map(item => {
        return {
            id : item.id,
            title: item.title,
            image: `http://localhost:8080/uploads/${item.image}`,  
        };
    });
    
    return response.json(serializedItems);
});


routes.post('/points', async (request, response) => {
    const {
        name,
        email,
        wpp,
        city,
        uf,
        latitude,
        longitude,
        items
    } = request.body;

    await knex('points').insert({
        name,
        image: 'image-fake',
        email,
        wpp,
        city,
        uf,
        latitude,
        longitude
    });

    return response.json({sucess: true})
});

export default routes